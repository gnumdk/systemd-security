# systemd-security

Small python script allowing to fix issues like: **Failed at step NAMESPACE spawning**

This works without enabling nesting in Proxmox.

Usage: python3 systemd_security.py service_name
