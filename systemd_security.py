#!/usr/bin/python3
import sys
import os
import subprocess

class Test:
    TODO=0
    TESTED=1
    MANDATORY=2
    USELESS=3

BOOL={
    'ProtectClock' : Test.TODO,
    'PrivateDevices' : Test.TODO,
    'ProtectHome' : Test.TODO,
    'ProtectHostname' : Test.TODO,
    'ProtectSystem' : Test.TODO,
    'PrivateTmp' : Test.TODO,
    'PrivateUsers' : Test.TODO,
    'ProtectControlGroups' : Test.TODO,
    'ProtectKernelLogs' : Test.TODO,
    'ProtectKernelModules' : Test.TODO,
    'ProtectKernelTunables' : Test.TODO,
    'LockPersonality' : Test.TODO,
    'MemoryDenyWriteExecute' : Test.TODO,
    'NoNewPrivileges' : Test.TODO,
    'RemoveIPC' : Test.TODO,
    'RestrictNamespaces' : Test.TODO,
    'RestrictRealtime' : Test.TODO,
    'RestrictSUIDSGID': Test.TODO
}

OTHERS={
    'ProtectProc:default' : Test.TODO,
    'ProcSubset:all' : Test.TODO,
    'ReadWritePaths' : Test.TODO,
    'ReadWriteDirectories' : Test.TODO,
    'NoExecPaths' : Test.TODO,
    'ExecPaths' : Test.TODO
}

def add_header(path):
    with open(path, 'w') as f:
        f.write("[Service]\n")

def add_bool_option(path, option):
    if BOOL[option] in [Test.USELESS, Test.TESTED]:
        return
    with open(path, 'a') as f:
        f.write("%s=false\n" % option)

def add_other_option(path, option):
    if OTHERS[option] in [Test.USELESS, Test.TESTED]:
        return
    with open(path, 'a') as f:
        split = option.split(':')
        if (len(split) > 1):
            f.write("%s=%s\n" % (split[0], split[1]))
        else:
            f.write("%s=\n" % option)

def mark_new_option_as_tested():
    for group in [BOOL, OTHERS]:
        for option in group.keys():
            if group[option] == Test.TODO:
                group[option] = Test.TESTED
                print("Testing %s: " % option, end="")
                return

def mark_tested_option_as(value):
    for group in [BOOL, OTHERS]:
        for option in group.keys():
            if group[option] == Test.TESTED:
                group[option] = value
                if value == Test.USELESS:
                    print("Not necessary")
                else:
                    print("Necessary")
                return

def all_tested():
    for group in [BOOL, OTHERS]:
        for option in group.keys():
            if group[option] == Test.TODO:
                return False
    return True


if len(sys.argv) < 2:
    print("Missing service argument")
    exit(0)

service=sys.argv[1]

if os.path.isfile("/lib/systemd/system/%s.service" % service) is False and\
   os.path.isfile("/etc/systemd/system/%s.service" % service) is False:
    print("Service %s missing in /lib/systemd/system" % service)
    exit(0)


os.makedirs("/etc/systemd/system/%s.service.d" % service, exist_ok=True)
path="/etc/systemd/system/%s.service.d/%s.conf" % (service, service)

# On teste toutes les options
add_header(path)
for option in BOOL.keys():
   add_bool_option(path, option)
for option in OTHERS.keys():
   add_other_option(path, option)
   
subprocess.run("systemctl daemon-reload", shell=True)
status = subprocess.run("systemctl restart %s" % service, shell=True, capture_output=True)
if status.returncode != 0:
    print('Unable to find a solution')
    os.remove(path) 
    exit(0)
   
# On recommence option par option
while all_tested() == False:
    mark_new_option_as_tested()
    add_header(path)
    for option in BOOL.keys():
        add_bool_option(path, option)
    for option in OTHERS.keys():
        add_other_option(path, option)
    subprocess.run("systemctl daemon-reload", shell=True)
    status = subprocess.run("systemctl restart %s" % service, shell=True, capture_output=True)
    if status.returncode != 0:
        mark_tested_option_as(Test.MANDATORY)
    else:
        mark_tested_option_as(Test.USELESS)

# Version finale
add_header(path)
for option in BOOL.keys():
   add_bool_option(path, option)
for option in OTHERS.keys():
   add_other_option(path, option)
subprocess.run("systemctl daemon-reload", shell=True)
status = subprocess.run("systemctl restart %s" % service, shell=True, capture_output=True)
if status.returncode != 0:
    print('Something goes wrong :(')
    exit(0)